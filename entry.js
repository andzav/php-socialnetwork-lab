$('h3').click(function (e) {
    e.preventDefault();
    if (e.target.text !== null) {
        if (e.target.text === "Sign Up") {
            $("#title").html('Please <a href="#">Log In</a>, or Sign Up');
            $("#formHolder").load("forms/regform.html");
        } else if (e.target.text === "Log In") {
            $("#title").html('Please Log In, or <a href="#" id="signUp">Sign Up</a>');
            $("#formHolder").load("forms/logform.html");
        }
    }
});

let regData = {};
let tempMail = "";

$('#formHolder').click(function (e) {
    let form = e.delegateTarget.firstElementChild;
    e.preventDefault();
    if (e.target.type === "submit") {
        formHandler(form, e.target.innerText);
    }
    if (e.target.tagName === "A") {
        form.children[1].remove();
        $('h3').hide();
        e.target.style.display = "none";
        form.children[2].innerText = "Restore";
    }
});

function formHandler(form, caption) {
    let data = {};
    for (let i = 0, length = form.elements.length - 1; i < length; i++) {
        let fieldName = form.elements[i].name;
        let fieldValue = form.elements[i].value;
        data[fieldName] = fieldValue;
        data["method"] = caption;
    }
    switch (data["method"]) {
        case "Login":
            loginHandler(data);
            break;
        case "Register":
            registerHandler(data);
            break;
        case "Restore":
            restoreHandler(data);
            break;
        case "Update password":
            updatePwdHandler(data);
            break;
        case "Confirm":
            confirmEmail(data);
            break;
        default:
            return false;
    }
}

function loginHandler(data) {
    if (validateEmail(data["email"]) && data["pwd"].length >= 8 && data["pwd"].length <= 32) {
        data["method"] = "login";

        ///////////////////////////////////////API/////////////////////////////////////
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(data))
            .done(function(response){
                if(response['success']===1){
                    showAlert("Logged in successfully", "bg-success");
                    localStorage.setItem("SID", response['SID']);
                    localStorage.setItem('UID', response['id']);
                    window.location.href = "index.html";
                    location.reload();
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
                
            });
        ///////////////////////////////////////API/////////////////////////////////////
    } else {
        showAlert("Email or password is invalid", "bg-danger");
    }
}

function registerHandler(data) {
    let errormsg;
    if (!validateEmail(data["email"])) {
        showAlert("Email is invalid", "bg-danger");
        return false;
    } else
    if (!validateName(data["fname"])) {
        showAlert("First name is invalid", "bg-danger");
        return false;
    } else
    if (!validateName(data["lname"])) {
        showAlert("Last name is invalid", "bg-danger");
        return false;
    } else
    if (data["pwd"].length < 8 || data["pwd"].length > 32) {
        showAlert("Password isn't valid", "bg-danger");
        return false;
    } else
    if (data["pwd"] !== data["pwd_confirm"]) {
        showAlert("Passwords don't match", "bg-danger");
        return false;
    } else {
        regData = data;
        data['method']='registerUser';

        ///////////////////////////////////////API/////////////////////////////////////
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(data))
            .done(function(response){
                if(response['success']===1){
                    $("#title").html('Confirm your email');
                    $("#formHolder").load("forms/confirm.html");
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
            });
        ///////////////////////////////////////API/////////////////////////////////////
    }
}

function restoreHandler(data) {
    if (validateEmail(data["email"])) {
        showAlert("Visit your inbox", "bg-success");
        data['method'] = 'restorePwd';
        
        ///////////////////////////////////////API/////////////////////////////////////
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(data))
            .done(function(response){
                if(response['success']===1){
                    $("#formHolder").load("forms/recover.html");
                    tempMail = data["email"];
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
            });
        ///////////////////////////////////////API/////////////////////////////////////
    } else {
        showAlert("Email is invalid", "bg-danger");
    }
}

function updatePwdHandler(data) {
    if (data["confirmCode"].length !== 8 || !isInt(data["confirmCode"])) {
        showAlert("Confirmation code is invalid", "bg-danger");
    } else
    if (data["pwd"].length < 8 || data["pwd"].length > 32) {
        showAlert("Password isn't valid", "bg-danger");
        return false;
    } else
    if (data["pwd"] !== data["pwd_confirm"]) {
        showAlert("Passwords don't match", "bg-danger");
        return false;
    } else {
        data["email"] = tempMail;
        data['method'] = 'updatePwd';
        
        ///////////////////////////////////////API/////////////////////////////////////
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(data))
            .done(function(response){
                if(response['success']===1){
                    $("#formHolder").load("forms/logform.html");
                    showAlert("Recovery successful", "bg-success");
                    $('h3').show();
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
            });
        ///////////////////////////////////////API/////////////////////////////////////
    }
}

function confirmEmail(data) {
    if (data["confirmCode"].length === 8 && isInt(data["confirmCode"])) {
        let apiQuery = {'method':'confirmEmail', 'email':regData['email'], 'code': data['confirmCode']};
        
        ///////////////////////////////////////API/////////////////////////////////////
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
            .done(function(response){
                if(response['success']===1){
                    showAlert("Registration successful", "bg-success");
                    $("#title").html('Please Log In, or <a href="#" id="signUp">Sign Up</a>');
                    $("#formHolder").load("forms/logform.html");
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
            });
        ///////////////////////////////////////API/////////////////////////////////////
    } else {
        showAlert("Confirmation code is invalid", "bg-danger");
    }

}

function showAlert(text, bg_class) {
    let alertEl = $("#alert");
    $("#alert").finish();
    alertEl.text(text);
    alertEl.removeClass();
    alertEl.addClass(' alertbox ' + bg_class);
    alertEl.fadeIn(1000).delay(2000).fadeOut(1000);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateName(Name) {
    var re = /[A-Z][a-z]{1,15}/
    return re.test(Name);
}

function isInt(value) {
    var x;
    if (isNaN(value)) {
        return false;
    }
    x = parseFloat(value);
    return (x | 0) === x;
}