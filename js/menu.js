if (sessionStorage.getItem('last_page') === null) {
    sessionStorage.setItem('last_page', 'news');
    $('#container').load('content/news.html');
    $.getScript("js/handlers/news.js");
    location.hash = 'news';
} else {
    var lastPage = sessionStorage.getItem('last_page');
    location.hash = lastPage;
    $('#container').load('content/' + clearPath(lastPage) + '.html');
    $.getScript("js/handlers/" + clearPath(lastPage) + ".js");
    $(".active").removeClass("active");
    $('a[href="#' + clearPath(lastPage) + '"]').addClass("active");
}

var menu = document.getElementById("menu");

function menu_click(e) {
    e = e || window.event;
    if (e.target.localName === "a") {
        $(".active").removeClass("active");
        e.target.classList.add("active");
    }
}
menu.addEventListener('click', menu_click);

$('#menu #avatar').attr('src', 'img/avatar/'+localStorage.getItem("UID")+'.jpg');

function setName(){
    let apiQuery = {'method': 'getName', 'id': localStorage.getItem("UID")};
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            $('#name').text(response['fullname']);
        }
    });
}
setName();

$(window).on('hashchange', hashchanged);

function hashchanged(e) {
    let path = clearPath(location.hash.replace(/^#/, ''));
    if(path.length>1){
        if (path === 'logout') {
            let apiQuery = {'method':'logOff', 'SID':localStorage.getItem('SID')}
            $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
            .done(function(response){
                localStorage.removeItem('SID');
                localStorage.removeItem('UID');
                sessionStorage.clear();
                location.reload();
            });
        } else {
            if(!(path==clearPath(sessionStorage.getItem('last_page'))&&path==='messages')){
                $(".active").removeClass("active");
                $("a[href$='#"+path+"']").addClass("active");
                sessionStorage.setItem('last_page', location.hash.replace(/^#/, ''));
                $('#container').load('content/' + path + '.html');
                $.getScript("js/handlers/" + path + ".js");
                window.scrollTo(0, 0);
            }
        }   
    }
}

function showAlert(text, bg_class) {
    let alertEl = $("#alert");
    $("#alert").finish();
    alertEl.text(text);
    alertEl.removeClass();
    alertEl.addClass(' alertbox ' + bg_class);
    alertEl.fadeIn(1000).delay(2000).fadeOut(1000);
}

function printScripts() {
    var scripts = document.getElementsByTagName("script");
    for (var i = 0; i < scripts.length; i++) {
        if (scripts[i].src) console.log(i, scripts[i].src)
        else console.log(i, scripts[i].innerHTML)
    }
}

function clearPath(s){
    if(s.indexOf("profile")!==-1) return "profile";
    if(s.indexOf("friends")!==-1) return "friends";
    if(s.indexOf("news")!==-1) return "news";
    if(s.indexOf("messages")!==-1) return "messages";
    if(s.indexOf("settings")!==-1) return "settings";
    if(s.indexOf("logout")!==-1) return "logout";
    return "";
}