let form = document.getElementById('settings');

function loadSettings() {
    //API GET SETTING FROM SERVER
    let apiQuery = {'method':'getSettings', 'SID':localStorage.getItem('SID')};
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            $("#FNameInput").val(response["fname"]);
            $("#LNameInput").val(response["lname"]);
            $("#country").val(response["country"]);
            $("#city").val(response["city"]);
            $("#phone").val(response["tel"]);
            $("input[name=wallVis][value=" + response["wallVis"] + "]").attr('checked', 'checked');
            $("input[name=phoneVis][value=" + response["phoneVis"] + "]").attr('checked', 'checked');
            $("input[name=messAcc][value=" + response["messAcc"] + "]").attr('checked', 'checked');
            $("input[name=gender][value=" + response["gender"] + "]").attr('checked', 'checked');
            showAlert('Settings loaded successfuly','bg-success')
        }else{
            showAlert('Error occured while processing your data','bg-danger')
        }
    });
    
}
loadSettings();

form.addEventListener('submit', submitSettings);
function submitSettings(e){
    e.preventDefault();
    let settings = {
        "fname": $("#FNameInput").val(),
        "lname": $("#LNameInput").val(),
        "country": $("#country").val(),
        "city": $("#city").val(),
        "tel": $("#phone").val(),
        "wallVis":  $("input[name=wallVis]:checked").attr("value"),
        "phoneVis": $("input[name=phoneVis]:checked").attr("value"),
        "messAcc": $("input[name=messAcc]:checked").attr("value"),
        "gender": $("input[name=gender]:checked").attr("value")
    };
    let apiQuery = {'method':'setSettings', 'settings':settings, 'SID':localStorage.getItem('SID')};
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            showAlert('Settings applied successfuly','bg-success')
        }else{
            showAlert('Error occured while processing your data','bg-danger')
        }
    });
}