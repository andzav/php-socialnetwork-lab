$('textarea').each(function () {
    this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
}).on('input', function () {
    this.style.height = 'auto';
    this.style.height = (this.scrollHeight) + 'px';
});

$('#dialogList').perfectScrollbar({
    wheelSpeed: 2
});
$("#dialogList").scrollTop(0);


document.getElementById('dialogList').addEventListener('click', loadMsg);

function loadMsg(e) {
    if (e.target.parentNode.id === "friend" || e.target.id === "friend") {
        let selectedFriend = e.target.id === "friend" ? e.target : e.target.parentNode;
        userID = selectedFriend.attributes["uid"].value;
        location.hash = 'messages?id='+userID;
        sessionStorage.setItem('last_page', location.hash.replace(/^#/, ''));
        loadMessages(selectedFriend.attributes["uid"].value);
        $(".selectedFriend").removeClass("selectedFriend");
        selectedFriend.classList.add("selectedFriend");
    }
}
function loadDialogs(){
    let apiQuery = {'method':'getDialogs', 'SID':localStorage.getItem('SID')};
    
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            let DialogList = response['dialogs'];
            DialogList.forEach(function(el){
                let element = '<div id="friend" uid="'+el['id']+'"> <img class="roundimg" src="img/avatar/'+el['id']+'.jpg"/> <span class="ml-3">'+el['fullname']+'</span> </div>';
                $('#dialogList').append(element);
            });
            userID = parseInt(window.location.hash.substr(13));
            if(!isNaN(userID)){
                if($('div[uid='+userID+']').length>0)
                    $('div[uid='+userID+']').addClass("selectedFriend");
                else{
                    let apiQuery = {'method': 'getName', 'id': userID};
                    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
                    .done(function(response){
                        if(response['success']===1){
                            let person = {'id': response['id'], 'fullname': response['fullname']};
                            let element = '<div id="friend" class="selectedFriend" uid="'+person['id']+'"> <img class="roundimg" src="img/avatar/'+person['id']+'.jpg"/> <span class="ml-3">'+person['fullname']+'</span> </div>';
                            $('#dialogList').prepend(element);
                        }else{
                            showAlert('Error occured while processing your data','bg-danger')
                        }
                    });
                }
            }else{
                $('#messageArea').hide();
            }
        }else{
            showAlert('Error occured while processing your data','bg-danger')
        }
    });
}
loadDialogs();

function loadMessages(recID){
    $('#selectedLink img').attr('src', 'img/avatar/'+recID+'.jpg');
    let apiQuery = {'method':'getMessages', 'recid':recID, 'SID':localStorage.getItem('SID')};
    $('#selectedLink').attr('href', '#profile?id='+recID);
    $('#history').empty();
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            let messages = response['messages'];
            messages.forEach(function(el){
                let messageTemp = '<div id="message" class="row justify-content-center my-3 py-3 mx-1 px-2"> <a href="#profile?id=' + el['id'] + '"><img class="roundimg mx-3" src="img/avatar/'+el['id']+'.jpg"/></a> <div id="body" class="col-8 col-sm-7">' + el['body'] + '</div><div class="col-1 col-sm-2 justify-content-end"> <p>' + el['timestamp'] + '</p></div></div>';
                $('#history').append(messageTemp);
            });
            $('#messageArea').show();
            $('#history').css("height", "auto");
            var messagesHeight = $('#history').height();
            $('#history').css("height", "80vh");
            $("#history").scrollTop(messagesHeight);
        }else{
            showAlert('Error occured while processing your data','bg-danger')
        }
    });    
}

$('button').click(function(){
    let messageBody = $('textarea').val();
    $('textarea').val('');
    if(messageBody.length>0){
        let apiQuery = {'method':'sendMessage', 'recid':userID, 'msg':messageBody, 'SID':localStorage.getItem('SID')};
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
        .done(function(response){
            if(response['success']===1){
                loadMessages(userID);
            }else{
                showAlert("Message cannot be sent", "bg-danger");
            }
        }); 
        console.log(apiQuery);
    }
});

function showAlert(text, bg_class) {
    let alertEl = $("#alert");
    $("#alert").finish();
    alertEl.text(text);
    alertEl.removeClass();
    alertEl.addClass(' alertbox ' + bg_class);
    alertEl.fadeIn(1000).delay(2000).fadeOut(1000);
}