$('textarea').each(function () {
    this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
}).on('input', function () {
    this.style.height = 'auto';
    this.style.height = (this.scrollHeight) + 'px';
});


function fillShortInfo(info) {
    $('.avatar').attr('src','img/avatar/'+info['id']+'.jpg');
    $('#status').text(info['status']);
    $('#fullname').text(info['fullname']);
    if (info['status'] === 'online') {
        $('#status').addClass('text-success');
    } else {
        $('#status').addClass('text-danger');
    }
    $('#message').attr('href', '#messages?id=' + info['id']);
    let elem = '';
    if (info['gender'] !== undefined && info['gender'].length>0) elem += '<p>Gender: ' + info['gender'] + '</p>';
    if (info['country'] !== undefined && info['country'].length>0) elem += '<p>Country: ' + info['country'] + '</p>';
    if (info['city'] !== undefined && info['city'].length>0) elem += '<p>City: ' + info['city'] + '</p>';
    if (info['tel'] !== undefined && info['tel'].length>0) elem += '<p>Phone: ' + info['tel'] + '</p>';
    if (elem==='') elem += '<p>No info here yet</p>';
    $('#shortInfo').html(elem);
}

function loadPage(generalInfo){
    if (isNaN(userID) || userID == localStorage.getItem('UID')) {
        $('#message').remove();
        $('#add').remove();
        $('#delete').remove();
        $('#unsubscribe').remove();
        $('#unblock').remove();
        $('#block').remove();
        generalInfo['status'] = 'online';
        fillShortInfo(generalInfo);
    } else {
        $('#avatar_change').remove();
        $('.input-group').remove();
        if (generalInfo['friendship'] === 0) {
            if (localStorage.getItem('UID') == generalInfo['action_id']) {
                if (generalInfo['messAcc'] === 0) $('#message').remove();
                $('#add').remove();
                $('#delete').remove();
                $('#unblock').remove();
            } else {
                if (generalInfo['messAcc'] === 0) $('#message').remove();
                $('#unsubscribe').remove();
                $('#delete').remove();
                $('#unblock').remove();
            }
            fillShortInfo(generalInfo);
        } else if (generalInfo['friendship'] === 1) {
            $('#unsubscribe').remove();
            $('#add').remove();
            $('#unblock').remove();
            fillShortInfo(generalInfo);
        } else if (generalInfo['friendship'] === 2) {
            if (localStorage.getItem('UID') == generalInfo['action_id']) {
                $('#message').remove();
                $('#add').remove();
                $('#delete').remove();
                $('#unsubscribe').remove();
                fillShortInfo(generalInfo);
                $('#block').remove();
            } else {
                $('#message').remove();
                $('#add').remove();
                $('#delete').remove();
                $('#unsubscribe').remove();
                $('#unblock').remove();
                $('#wallPosts').remove();
                $('#shortInfo').remove();
                $("#profileLeft").append('<h3 class="text-danger">You were blocked by this user</h3>');
            }
        } else if (generalInfo['friendship'] === undefined) {
            fillShortInfo(generalInfo);
            if (generalInfo['messAcc'] === 0) $('#message').remove();
            $('#delete').remove();
            $('#unsubscribe').remove();
            $('#unblock').remove();
        }
    }
}

function loadPersonalPage() {
    userID = parseInt(window.location.hash.substr(12));
    //API CALL TO RETURN ALL INFO
    let apiQuery = {
        'method': 'getProfile',
        'id': userID,
        'SID': localStorage.getItem('SID')
    };
    if(isNaN(userID)){
        apiQuery['id']=localStorage.getItem('UID');
    }
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            loadPage(response);
        }else{
            showAlert('Error occured while processing your data','bg-danger')
        }
    });

    
    
    apiQuery = {
        'method': 'getWallPosts',
        'id': userID,
        'SID': localStorage.getItem('SID')
    };
    if(isNaN(userID)){
        apiQuery['id']=localStorage.getItem('UID');
    }
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
            .done(function(response){
                if(response['success']===1){
                    let wallPosts=response['wallPosts'];
                    if(wallPosts!==undefined){
                        $('#news').empty();
                        if (wallPosts.length > 0) {
                            for (let i = 0; i < wallPosts.length; i++) {
                                appendNewsEl(wallPosts[i]);
                            }
                        }
                    }else{
                        $("#wallPosts").remove();
                    }
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
            });
}
loadPersonalPage();

function appendNewsEl(newsDetails) {
    let statusColor = "text-danger";
    if (newsDetails["status"] === "online") statusColor = "text-success";
    let url='<a href="#profile">';
    if(!isNaN(newsDetails['id'])) url= '<a href="#profile?id=' + newsDetails['id'] + '">';
    let newsEl = '<div id="post" class="row my-3 py-3 px-2 border border-primary">'+url+'<img class="roundimg mx-3" src="img/avatar/'+newsDetails['id']+'.jpg"/></a> <div class="col-4 col-lg-5 justify-content-left"> <h4>' + newsDetails['fullname'] + '</h4> <p class="' + statusColor + '">' + newsDetails['status'] + '</p></div><div class="col-4 col-lg-4 vertical-center justify-content-end"> <p>' + newsDetails["timestamp"] + '</p></div><div class="col-2 col-lg-2 clearfix"> </div><div id="body" class="col-9 col-lg-9">' + newsDetails["body"] + '</div></div>';
    $("#news").append(newsEl);
}

$('button').click(function (e) {
    let caption = e.target.textContent.toLowerCase();
    let apiQuery;
    if (caption !== 'post' && caption !== 'update picture') {
        caption += 'Friend';
        apiQuery = {'method':caption, 'id':userID, 'SID':localStorage.getItem('SID')};
    }else if (caption === 'post'){
        let messageBody = $('textarea').val();
        if(messageBody.length>0){
            apiQuery = {'method':caption, 'wallBody':messageBody, 'SID':localStorage.getItem('SID')};
        }
    }else if (caption === 'update picture'){
        var blobFile = $('#avatar_file')[0].files[0];
        var formData = new FormData();
        formData.append("avatar", blobFile);
        formData.append("SID", localStorage.getItem('SID'));
        formData.append("UID", localStorage.getItem('UID'));
        formData.append('method', 'updateAvatar');
        
        $.ajax({
           url: "https://testserverminer.000webhostapp.com/index123.php",
           type: "POST",
           data: formData,
           processData: false,
           contentType: false,
           success: function(response) {
               showAlert('Avatar updated successfully', 'bg-success');
           }
        });
    }
    if(apiQuery!==undefined){
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
            .done(function(response){
                if(response['success']===1){
                    loadPersonalPage();
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
            });
    }
});
