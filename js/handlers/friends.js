function addFriend(friendInfo){
    let statusColor = "text-danger";
    if(friendInfo["status"]==="online") statusColor="text-success";
    let friend = '<div id="person" class="row my-3 py-3"> <a href="#profile?id='+friendInfo['id']+'"><img class="roundimg mx-3" src="img/avatar/'+friendInfo['id']+'.jpg"/></a> <div class="col-3 col-sm-3 col-md-4"> <h4>'+friendInfo['fullname']+'</h4> <p class="'+statusColor+'">'+friendInfo['status']+'</p></div><div class="col-4 col-sm-5 col-md-4 col-lg-5 vertical-center justify-content-end"> <a href="#messages?id='+friendInfo['id']+'" type="button" class="btn btn-primary">Message</a> </div><div class="col-1 col-sm-1 vertical-center justify-content-center"> <button type="button" uid="'+friendInfo['id']+'" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i></button> </div></div>';
    $('#friends').append(friend);
}

function addRequest(requestInfo){
    let msgBtn = '';
    let statusColor = "text-danger";
    if(requestInfo["status"]==="online") statusColor="text-success";
    if(requestInfo["messAcc"]===1) msgBtn = '<a href="#messages?id='+requestInfo['id']+'" type="button" class="btn btn-primary">Message</a>';
    let request = '<div id="person" class="row my-3 py-3"> <a href="#profile?id='+requestInfo['id']+'"><img class="roundimg mx-3" src="img/avatar/'+requestInfo['id']+'.jpg"/></a> <div class="col-3 col-sm-3 col-md-4"> <h4>'+requestInfo['fullname']+'</h4> <p class="'+statusColor+'">'+requestInfo['status']+'</p></div><div class="col-4 col-sm-5 col-md-4 col-lg-5 vertical-center justify-content-end"> '+msgBtn+' </div><div class="col-1 col-sm-1 vertical-center justify-content-center"> <div class="btn-group-lg btn-group-vertical"> <button type="button" uid="'+requestInfo['id']+'" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button><br/> <button type="button" uid="'+requestInfo['id']+'" class="btn btn-danger"><i class="fa fa-ban" aria-hidden="true"></i></button> </div></div></div>';
    $('#requests').append(request);
}

$('#friendsList').on('click', 'button', function(e) {
    if(e.target.localName==='button'){
        let method = selectAction(e.target.firstChild.classList[1]);
        let apiQuery = {'method': method, 'id': e.target.attributes["uid"].value, 'SID':localStorage.getItem('SID')};
        $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
        .done(function(response){
            if(response['success']===1){
                location.reload();
            }else{
                showAlert('Error occured while processing oparation','bg-danger')
            }
        });
    }
});

function selectAction(s){
    if(s==='fa-times') return "deleteFriend";
    if(s==='fa-plus') return "addFriend";
    if(s==='fa-ban') return "blockFriend";
}

function loadFriends(){

    let apiQuery = {'method':'getFriendsList', 'SID':localStorage.getItem('SID')};
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            friends = response['friends'];
            friends.length>0 ? $("#friendsCount").text('Friends ('+friends.length+'):') : $('#friendsCount').remove();
            friends.forEach(function(friend){
               addFriend(friend);
            });
        }else{
            showAlert('Error occured while processing your data','bg-danger')
        }
    });
    
    apiQuery = {'method':'getRequestList', 'SID':localStorage.getItem('SID')};
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
    .done(function(response){
        if(response['success']===1){
            requests = response['friends'];
            requests.length>0 ? $("#requestCount").text('Requests ('+requests.length+'):') : $('#requestCount').remove();
            requests.forEach(function(request){
               addRequest(request);
            });
        }else{
            showAlert('Error occured while processing your data','bg-danger')
        }
    });
}

loadFriends();