function loadNews() {
    //API CALL LOAD NEWS ARRAY (100 LATEST)
    let apiQuery = {'method': 'getNews', 'SID':localStorage.getItem('SID')};
    $.post("https://testserverminer.000webhostapp.com/index123.php", JSON.stringify(apiQuery))
            .done(function(response){
                if(response['success']===1){
                    let wallPosts=response['wallPosts'];
                    if(wallPosts!==undefined){
                        $('#newsList').empty();
                        if (wallPosts.length > 0) {
                            for (let i = 0; i < wallPosts.length; i++) {
                                appendNewsEl(wallPosts[i]);
                            }
                        }
                    }else{
                        $("#newsList").html('<h3>No news yet</h3>');
                    }
                }else{
                    showAlert('Error occured while processing your data','bg-danger')
                }
            });
}

function appendNewsEl(newsDetails) {
    let statusColor = "text-danger";
    if (newsDetails["status"] === "online") statusColor = "text-success";
    
    let newsEl = '<div id="post" class="row my-3 py-3 mx-1 px-2 border border-primary"> <a href="#profile?id=' + newsDetails['id'] + '"><img class="roundimg mx-3" src="img/avatar/'+newsDetails['id']+'.jpg"/></a> <div class="col-4 col-lg-5 justify-content-left"> <h4>' + newsDetails['fullname'] + '</h4> <p class="' + statusColor + '">' + newsDetails['status'] + '</p></div><div class="col-4 col-lg-4 vertical-center justify-content-end"> <p>' + newsDetails["timestamp"] + '</p></div><div class="col-2 col-lg-2 clearfix"> </div><div id="body" class="col-9 col-lg-9">' + newsDetails["body"] + '</div></div>';
    
    $("#newsList").append(newsEl);
}

loadNews();