<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-Type: application/json');
    function isValidJSON($str) {
       json_decode($str);
       return json_last_error() == JSON_ERROR_NONE;
    }
    
    $json_params = file_get_contents("php://input");

    if (strlen($json_params) > 0 && isValidJSON($json_params))
      $_POST = json_decode($json_params, true);
    
    //error_reporting(0);

    if(isset($_POST)){

        $user = 'id1775457_kappa';
        $pass = 'kappa123';

        $dsn = "mysql:host=localhost;dbname=id1775457_socialdb;charset=utf8";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_WARNING, //PDO::ERRMODE_SILENT
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $pdo = new PDO($dsn, $user, $pass, $opt);
        
        function getFullName($pdo, $id){
            $sql = "SELECT * FROM settings WHERE id=$id";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            return $data[0]['fname'].' '.$data[0]['lname'];
        }
        
        function getMessAcc($pdo, $id){
            $sql = "SELECT * FROM settings WHERE id=$id";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            return $data[0]['messAcc'];
        }

        function getStatus($pdo, $id){
            $sql = "SELECT * FROM users WHERE id=$id";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            return $data[0]['status'];
        }
        
        if($_POST['method']=="updateAvatar"){
            $uploaddir = getcwd().'/img/avatar/';
            $uploadfile = $uploaddir.basename($_POST["UID"].'.jpg');

            if (move_uploaded_file($_FILES['avatar']['tmp_name'], $uploadfile)) {
                $response['success'] = 1;
            } else {
                $response['success'] = 0;
            }
            
            echo json_encode($response);
        }

        //LOGIN METHODS
        if($_POST['method']=='login'){
            $salt = hash('sha256',$_POST['email'].$_POST['pwd']);
            $pwd = hash('sha256', $_POST['pwd'].$salt);
            
            $sql = "SELECT * FROM users WHERE email='".$_POST['email']."' AND pwd='$pwd'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            if(count($data)==1){
                $milliseconds = round(microtime(true) * 1000);
                $SID = hash('md5', $_SERVER['REMOTE_ADDR'].$milliseconds);
                $sql = "UPDATE users SET SID='$SID', status='online', ip='".$_SERVER['REMOTE_ADDR']."' WHERE email='".$data[0]['email']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $response['success'] = 1;
                $response['SID'] = $SID;
                $response['id'] = $data[0]['id'];
            }else{
                $response['success'] = 0;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='registerUser'){
            $confirmCode = rand(10000000,99999999);
            $sql = "DELETE FROM unconfirmed WHERE email='".$_POST['email']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
                
            $sql = "INSERT INTO unconfirmed (fname, lname, email, pwd, code) VALUES ( '".$_POST['fname']."', '".$_POST['lname']."', '".$_POST['email']."', '".$_POST['pwd']."', ".$confirmCode.")";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            
            $to = $_POST['email'];
            $subject = "Registration confirmation";
            $txt = "Your confirmation code is ".$confirmCode;
            $headers = "From: noreply@nulp.com";

            mail($to,$subject,$txt,$headers);
            
            $response['success'] = 1;
            echo json_encode($response);
        }

        if($_POST['method']=='confirmEmail'){
            $sql = "SELECT * FROM unconfirmed WHERE email='".$_POST['email']."' AND code=".$_POST['code'];
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            if(count($data)==1){
                $fname = $data[0]['fname'];
                $lname = $data[0]['lname'];
                $sql = "DELETE FROM unconfirmed WHERE email='".$_POST['email']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $salt = hash('sha256',$data[0]['email'].$data[0]['pwd']);
                $pwd = hash('sha256', $data[0]['pwd'].$salt);
                $sql = "INSERT INTO users (email, pwd, salt) VALUES ( '".$data[0]['email']."', '".$pwd."', '".$salt."')";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $sql = "SELECT * FROM users WHERE email='".$_POST['email']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                
                $sql = "INSERT INTO settings (id, fname, lname) VALUES ( '".$data[0]['id']."', '$fname', '$lname')";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $response['success'] = 1;
            }else{
                $response['success'] = 0;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='restorePwd'){
            $sql = "SELECT * FROM users WHERE email='".$_POST['email']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            if(count($data)==1){
                $recoveryCode = rand(10000000,99999999);
                $to = $_POST['email'];
                
                $sql = "INSERT INTO pwdReset (email, code) VALUES ( '".$_POST['email']."', ".$recoveryCode.")";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $subject = "Password recovey";
                $txt = "Your recovery code is ".$recoveryCode;
                $headers = "From: noreply@nulp.com";

                mail($to,$subject,$txt,$headers);
                $response['success'] = 1;
            }else{
                $response['success'] = 0;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='updatePwd'){
            $sql = "SELECT * FROM pwdReset WHERE email='".$_POST['email']."' AND code=".$_POST['confirmCode'];
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            if(count($data)==1){
                $sql = "DELETE FROM pwdReset WHERE email='".$_POST['email']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $salt = hash('sha256',$data[0]['email'].$_POST['pwd']);
                $pwd = hash('sha256', $_POST['pwd'].$salt);
                $sql = "UPDATE users SET pwd='$pwd', salt='$salt' WHERE email='".$data[0]['email']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $response['success'] = 1;
            }else{
                $response['success'] = 0;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='logOff'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            if(count($data)==1){
                $sql = "UPDATE users SET SID='', ip='', status='Offline' WHERE email='".$data[0]['email']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $response['success'] = 1;
            }else{
                $response['success'] = 0;
            }
            echo json_encode($response);
        }

        //PROFILE METHODS
        if($_POST['method']=='post'){
            $milliseconds = round(microtime(true) * 1000);
            $response['success'] = 0;
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            if(count($data)==1){
                $sql = "INSERT INTO wall (sender, body, timestamp) VALUES ( '".$data[0]['id']."', '".$_POST['wallBody']."', '".date('Y-m-d H:i:s')."')";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $response['success'] = 1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='getName'){
            $sql = "SELECT * FROM users WHERE id='".$_POST['id']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success'] = 0;
            if(count($data)==1){
                $response['id'] = $data[0]['id'];
                $sql = "SELECT * FROM settings WHERE id='".$data[0]['id']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                $response['fullname'] = $data[0]['fname'].' '.$data[0]['lname'];
                $response['success'] = 1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='getProfile'){
            
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $data = $stm->fetchAll();
            $stm->execute();
            $data = $stm->fetchAll();
            $caller = $data[0]['id'];
            
            $sql = "SELECT * FROM users WHERE id='".$_POST['id']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success'] = 0;
            if(count($data)==1){
                $response['id'] = $data[0]['id'];
                $response['status'] = $data[0]['status'];
                $phone = $data[0]['tel'];
                $sql = "SELECT * FROM settings WHERE id='".$data[0]['id']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                
                $response['fullname'] = $data[0]['fname'].' '.$data[0]['lname'];
                $response['gender'] = $data[0]['gender'];
                $response['country'] = $data[0]['country'];
                $response['city'] = $data[0]['city'];
                $response['messAcc'] = $data[0]['messAcc'];
                $phoneVis = $data[0]['phoneVis'];
                $sql = "SELECT * FROM friends WHERE (id1=$caller AND id2=".$response['id'].") OR (id1=".$response['id']." AND id2=$caller)";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                if(count($data)==1){
                    $response['friendship'] = $data[0]['friendship'];
                    $response['action_id'] = $data[0]['action_id'];
                    if($phoneVis==1&&$response['friendship']!=2){
                        $response['tel']=$phone;
                    }
                    if($phoneVis==0&&$response['friendship']==1){
                        $response['tel']=$phone;
                    }
                }
                if($phoneVis==1&&!isset($response['friendship'])){
                    $response['tel']=$phone;
                }
                if($caller==$_POST['id']){
                    $response['tel']=$phone;
                }
                $response['success'] = 1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='getWallPosts'){
            $id = $_POST['id'];
            $SID = $_POST['SID'];
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $caller = $data[0]['id'];
            
            $sql = "SELECT * FROM users WHERE id='".$_POST['id']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $status = $data[0]['status'];
            $sql = "SELECT * FROM settings WHERE id=$id";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $fullName = $data[0]['fname'].' '.$data[0]['lname'];
            $wallVis = $data[0]['wallVis'];
            
            $sql = "SELECT * FROM friends WHERE (id1=$caller AND id2=$id) OR (id1=$id AND id2=$caller)";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success'] = 1;
            $loadWall = false;
            
            if(count($data)==1){
                $response['friendship'] = $data[0]['friendship'];
                $response['action_id'] = $data[0]['action_id'];
                if($wallVis==1&&$response['friendship']!=2){
                    $loadWall = true;
                }
                if($wallVis==0&&$response['friendship']==1){
                    $loadWall = true;
                }
            }
            if($wallVis==1&&!isset($response['friendship'])){
                $loadWall = true;
            }
            if($caller==$id){
                $loadWall = true;
            }
            if($loadWall){
                $sql = "SELECT * FROM wall WHERE sender = $id ORDER BY timestamp DESC";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $wallPosts=array();
                $data = $stm->fetchAll();
                foreach ($data as $post) {
                    $wallPost['id'] = $post['sender'];
                    $wallPost['fullname'] = $fullName;
                    $wallPost['status'] = $status;
                    $wallPost['body'] = $post['body'];
                    $wallPost['timestamp'] = $post['timestamp'];
                    array_push($wallPosts, $wallPost);
                }
                $response['wallPosts'] = $wallPosts;
                $response['success'] = 1;
            }
            echo json_encode($response);
            
        }

        if($_POST['method']=='addFriend'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $id1 = $data[0]['id'];
                $id2 = $_POST['id'];
                
                $sql = "SELECT * FROM friends WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 )";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                if(count($data)==1){
                    if($data[0]['friendship']==0&&$data[0]['action_id']!=$id1){
                        $sql = "UPDATE friends SET friendship = 1, action_id = $id1 WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 )";
                        $stm = $pdo->prepare($sql);
                        $stm->execute();
                    }
                }else{
                    $sql = "INSERT INTO friends (id1, id2, friendship, action_id) VALUES ( $id1, $id2, 0, $id1 )";
                    $stm = $pdo->prepare($sql);
                    $stm->execute();
                }
                $response['success']=1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='deleteFriend'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $id1 = $data[0]['id'];
                $id2 = $_POST['id'];
                
                $sql = "SELECT * FROM friends WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 ) AND friendship=1";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                if(count($data)==1){
                    $sql = "UPDATE friends SET friendship = 0, action_id = $id2 WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 )";
                    $stm = $pdo->prepare($sql);
                    $stm->execute();
                    $response['success']=1;
                }
            }
            echo json_encode($response);
        }

        if($_POST['method']=='unsubscribeFriend'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $id1 = $data[0]['id'];
                $id2 = $_POST['id'];
                
                $sql = "SELECT * FROM friends WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 ) AND friendship=0";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                if(count($data)==1){
                    if($data[0]['action_id']==$id1){
                        $sql = "DELETE FROM friends WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 )";
                        $stm = $pdo->prepare($sql);
                        $stm->execute();
                        $response['success']=1;
                    }
                }
            }
            echo json_encode($response);
        }

        if($_POST['method']=='unblockFriend'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $id1 = $data[0]['id'];
                $id2 = $_POST['id'];
                
                $sql = "SELECT * FROM friends WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 ) AND friendship=2";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                if(count($data)==1){
                    if($data[0]['action_id']==$id1){
                        $sql = "DELETE FROM friends WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 )";
                        $stm = $pdo->prepare($sql);
                        $stm->execute();
                    }
                    $response['success']=1;
                }
            }
            echo json_encode($response);
        }

        if($_POST['method']=='blockFriend'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $id1 = $data[0]['id'];
                $id2 = $_POST['id'];
                
                $sql = "SELECT * FROM friends WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 )";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                if(count($data)==1){
                    if($data[0]['friendship']!=2){
                        $sql = "UPDATE friends SET friendship = 2, action_id = $id1 WHERE ( id1=$id1 AND id2=$id2 ) OR ( id1=$id2 AND id2=$id1 )";
                        $stm = $pdo->prepare($sql);
                        $stm->execute();
                        $response['success']=1;
                    }
                }else{
                    $sql = "INSERT INTO friends (id1, id2, friendship, action_id) VALUES ($id1, $id2, 2, $id1)";
                    $stm = $pdo->prepare($sql);
                    $stm->execute();
                    $response['success']=1;
                }
            }
            echo json_encode($response);
        }

        //FRIENDS PAGE
        if($_POST['method']=='getFriendsList'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $caller = $data[0]['id'];
                $sql = "SELECT * FROM friends WHERE ( id1=$caller OR id2=$caller ) AND friendship = 1";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $friends = array();
                $data = $stm->fetchAll();
                foreach ($data as $friend) {
                    $friendEl['id'] = $friend['id1']==$caller ? $friend['id2'] : $friend['id1'];
                    $friendEl['fullname'] = getFullName($pdo, $friendEl['id']);
                    $friendEl['status'] = getStatus($pdo, $friendEl['id']);
                    array_push($friends, $friendEl);
                }
                $response['friends'] = $friends;
                $response['success'] = 1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='getRequestList'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $caller = $data[0]['id'];
                $sql = "SELECT * FROM friends WHERE ( id1=$caller OR id2=$caller ) AND friendship = 0 AND action_id<>$caller";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $friends = array();
                $data = $stm->fetchAll();
                foreach ($data as $friend) {
                    $friendEl['id'] = $friend['id1']==$caller ? $friend['id2'] : $friend['id1'];
                    $friendEl['fullname'] = getFullName($pdo, $friendEl['id']);
                    $friendEl['status'] = getStatus($pdo, $friendEl['id']);
                    array_push($friends, $friendEl);
                }
                $response['friends'] = $friends;
                $response['success'] = 1;
            }
            echo json_encode($response);
        }

        //MESSAGES PAGE
        if($_POST['method']=='getDialogs'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $caller = $data[0]['id'];
                $sql = "SELECT * FROM messages WHERE sender = $caller OR reciever = $caller ";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $dialogs = array();
                $data = $stm->fetchAll();
                forEach($data as $dialog) {
                    $el['id'] = $dialog['sender']==$caller ? $dialog['reciever'] : $dialog['sender'];
                    $el['fullname'] = getFullName($pdo, $el['id']);
                    array_push($dialogs, $el);
                }
                $dialogs = array_map("unserialize", array_unique(array_map("serialize", $dialogs)));
                $response['dialogs']=$dialogs;
                $response['success']=1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='getMessages'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $caller = $data[0]['id'];
                $reciever = $_POST['recid'];
                $sql = "SELECT * FROM messages WHERE ( sender = $caller AND reciever = $reciever ) OR ( sender = $reciever AND reciever = $caller ) ";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $messages = array();
                $data = $stm->fetchAll();
                forEach($data as $message) {
                    $el['id'] = $message['sender'];
                    $el['body'] = $message['body'];
                    $el['timestamp'] = $message['timestamp'];
                    array_push($messages, $el);
                }
                $response['messages']=$messages;
                $response['success']=1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='sendMessage'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $sender = $data[0]['id'];
                $reciever = $_POST['recid'];
                $msg = $_POST['msg'];
                $sendMessage = false;
                $messAcc = getMessAcc($pdo, $reciever);
                
                $sql = "SELECT * FROM friends WHERE ( id1=$sender AND id2=$reciever ) OR ( id1=$reciever AND id2=$sender )";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                if(count($data)==1){
                    $friendship = $data[0]['friendship'];
                    if($messAcc==1&&$friendship!=2){
                        $sendMessage = true;
                    }
                    if($messAcc==0&&$friendship==1){
                        $sendMessage = true;
                    }
                }
                if($messAcc==1&&!isset($friendship)){
                    $sendMessage = true;
                }
                if($sender==$reciever){
                    $sendMessage = true;
                }
                if($sendMessage){
                    $sql = "INSERT INTO messages (sender, reciever, body) VALUES ( $sender, $reciever, '$msg' )";
                    $stm = $pdo->prepare($sql);
                    $stm->execute();
                    $response['success']=1;
                }
            }
            echo json_encode($response);
        }

        //NEWS PAGE
        if($_POST['method']=='getNews'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $response['success']=0;
            if(count($data)==1){
                $caller = $data[0]['id'];
                $sql = "SELECT * FROM friends WHERE ( id1=$caller OR id2=$caller ) AND friendship = 1";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $friends = array();
                $data = $stm->fetchAll();
                foreach ($data as $friend) {
                    $friendEl['id'] = $friend['id1']==$caller ? $friend['id2'] : $friend['id1'];
                    array_push($friends, $friendEl['id']);
                }
                if(count($friends)==0) $friends = [0];
                $sql = "SELECT * FROM wall WHERE sender IN (" . implode(',', array_map('intval', $friends)) . ") ORDER BY timestamp DESC";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $wallPosts=array();
                $data = $stm->fetchAll();
                foreach ($data as $post) {
                    $wallPost['id'] = $post['sender'];
                    $wallPost['fullname'] = getFullName($pdo, $wallPost['id']);
                    $wallPost['status'] = getStatus($pdo, $wallPost['id']);
                    $wallPost['body'] = $post['body'];
                    $wallPost['timestamp'] = $post['timestamp'];
                    array_push($wallPosts, $wallPost);
                }
                $response['wallPosts'] = $wallPosts;
                $response['success'] = 1;
            }
            echo json_encode($response);
        }

        //SETTINGS PAGE
        if($_POST['method']=='getSettings'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $caller = $data[0]['id'];
            $response['success'] = 0;
            $response['tel'] = $data[0]['tel'];
            if(count($data)==1){
                $sql = "SELECT * FROM settings WHERE id='".$data[0]['id']."'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $data = $stm->fetchAll();
                
                $response['fname'] = $data[0]['fname'];
                $response['lname'] = $data[0]['lname'];
                $response['country'] = $data[0]['country'];
                $response['city'] = $data[0]['city'];
                $response['wallVis'] = $data[0]['wallVis'];
                $response['phoneVis'] = $data[0]['phoneVis'];
                $response['messAcc'] = $data[0]['messAcc'];
                $response['gender'] = $data[0]['gender'];
                
                $response['success'] = 1;
                $response['success'] = 1;
            }
            echo json_encode($response);
        }

        if($_POST['method']=='setSettings'){
            $sql = "SELECT * FROM users WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $data = $stm->fetchAll();
            $caller = $data[0]['id'];
            $sql = "UPDATE users SET tel='".$_POST['settings']['tel']."' WHERE SID='".$_POST['SID']."' AND ip='".$_SERVER['REMOTE_ADDR']."'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            if(count($data)==1){
                $fname = $_POST['settings']['fname'];
                $lname = $_POST['settings']['lname'];
                $country = $_POST['settings']['country'];
                $city = $_POST['settings']['city'];
                $wallVis = $_POST['settings']['wallVis'];
                $phoneVis = $_POST['settings']['phoneVis'];
                $messAcc = $_POST['settings']['messAcc'];
                $gender = $_POST['settings']['gender'];
                
                $sql = "UPDATE settings SET fname='$fname', lname='$lname', country='$country', city='$city', wallVis=$wallVis, phoneVis=$phoneVis, messAcc=$messAcc, gender='$gender' WHERE id=$caller";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                
                $response['success'] = 1;
            }
            echo json_encode($response);
        }
    }else{
        $response['success'] = 1;
        echo json_encode($response);
    }
?>